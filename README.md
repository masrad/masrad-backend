# masrad backend

The backend component for the MASRAD:platform, for archiving oral history. 


## Development setup

If you need to setup the MASRAD:platform for production, it's best to use the [masrad-deployment](https://gitlab.com/masrad/masrad-deployment/) project. If you are doing local development, follow the steps below: 

1. Clone this repository 
2. Using [pipenv](https://docs.pipenv.org/en/latest/), run `pipenv install`
3. Make sure that you have a [PostgreSQL](https://www.postgresql.org/download/) database installed and setup. 
4. Create a `local_settings.py` file in `masrad-backend/backend`, next to `settings.py`. The contents are: 
```
    DEBUG = True

    ALLOWED_HOSTS = ['127.0.0.1', ]

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': 'dbname', # fill in with the appropriate db name
            'USER': 'username', # fill in with the appropriate user name
            'PASSWORD': 'password', # fill in with the appropriate password. This field might be optional 
            'HOST': 'localhost',
            'PORT': 5432,
        }
    }
```

5. In the backend directory, run `python manage.py runserver`
6. Navigate to `127.0.0.1:8000` to see the development server!

