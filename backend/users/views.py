from django.contrib.auth.models import User, Permission
from django.shortcuts import get_object_or_404
from django.db.models import Q

from rest_framework.response import Response
from rest_framework import viewsets, permissions, generics, authentication

from .serializers import UserSerializer


class UsersViewSet(viewsets.ModelViewSet):
    queryset = User.objects.filter(is_active=True)
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated, )


    def retrieve(self, request, pk=None):
        """
        Override retrieve to check if we're asking for the current user
        """
        if pk == 'current':
            pk = request.user.id
            
        instance = User.objects.get(id=pk)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class EditorsViewSet(viewsets.ModelViewSet):
    try:
        queryset = User.objects.filter(Q(is_superuser=True) | Q(user_permissions=Permission.objects.get(codename='change_testimony'))).distinct()
    except:
        queryset = User.objects.none()
    serializer_class = UserSerializer
    #permission_classes = (permissions.IsAuthenticated, )

