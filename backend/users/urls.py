from django.urls import path
from .views import UsersViewSet, EditorsViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register('users', UsersViewSet)
router.register('editors', EditorsViewSet)
urlpatterns = router.urls