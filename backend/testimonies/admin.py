from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from modeltranslation.admin import TranslationAdmin

from .models import Person, Publisher, Collection, Subject, Restriction, Testimony
from transcripts.admin import TranscriptInline
from segmentation.admin import SegmentInline
from attachments.admin import AttachmentInline


class PersonAdmin(TranslationAdmin):
    list_display = ('name', 'phone', 'identifier', )
    readonly_fields = ('identifier', )


class PublisherAdmin(TranslationAdmin):
    pass


class CollectionAdmin(TranslationAdmin):
    pass


class SubjectAdmin(TranslationAdmin):
    pass


class RestrictionAdmin(TranslationAdmin):
    pass


class TestimonyAdmin(TranslationAdmin):
    fieldsets = (
        (_('Metadata'), {
            'fields': ('title', 'file', 'anonymized_file', 'contributor', 'creator', 'publisher', 'identifier', 'date', 'location',
                       'coverage', 'description', 'relation', 'subject', 'keywords', 'language', 'rights',  '_type', 'file_format')
        }),
        (_('Transcript'), {
            'classes': ('collapse',),
            'fields': ('transcript',),
        }),
        (_('Release data'), {
            #'classes': ('collapse',),
            'fields': ('release_form', 'publication_restriction', 'release_date'),
        }),
        (_('Publishing information'), {
            'fields': ('published', 'metadata_public', 'files_public', 'segmentation_public'),
        }),
        (_('Workflow status'), {
            'classes': ('collapse',),
            'fields': ('metadata_workflow', 'files_workflow', 'segmentation_workflow'),
        }),
    )
    inlines = [SegmentInline, AttachmentInline] #TranscriptInline
    list_display = ('title', 'creator', 'contributor', 'date', 'relation', 'created_by', 'last_changed_by')
    list_filter = ['contributor']
    readonly_fields = ('_type', 'file_format', 'identifier', )
    autocomplete_fields = ['keywords', 'subject', ]


admin.site.register(Person, PersonAdmin)
admin.site.register(Publisher, PublisherAdmin)
admin.site.register(Collection, CollectionAdmin)
admin.site.register(Restriction, RestrictionAdmin)
admin.site.register(Testimony, TestimonyAdmin)