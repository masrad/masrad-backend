from rest_framework import serializers

from .models import Testimony, Collection, Person, Publisher
from workflow.serializers import WorkflowSerializer
from tags.serializers import KeywordSerializer, SubjectSerializer


class CollectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Collection
        fields = '__all__'


class PublisherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publisher
        fields = '__all__'


class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = '__all__'


class TestimonySerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(format="%Y-%m-%d", required=False)
    #relation = CollectionSerializer(required=False)
    #creator = PersonSerializer(required=False)
    #contributor = PersonSerializer(required=False)
    #subject = SubjectSerializer(many=True, required=False)
    files_workflow = WorkflowSerializer(required=False)
    metadata_workflow = WorkflowSerializer(required=False)
    segmentation_workflow = WorkflowSerializer(required=False)
    file_size = serializers.ReadOnlyField()
    anonymized_file_size = serializers.ReadOnlyField()
    dc_identifier = serializers.CharField(required=False, source='identifier')

    class Meta:
        model = Testimony
        fields = ['id', 'title', 'file', 'file_size', 'anonymized_file', 'anonymized_file_size',
                  'contributor', 'creator', 'publisher', 'dc_identifier', 'date', 'location',
                  'coverage', 'description', 'relation', 'subject', 'file_format', 'language',
                  'rights', '_type', 'keywords', 'release_form', 'release_date',
                  'created', 'modified', 'published', 'files_public', 'metadata_public',
                  'segmentation_public', 'files_workflow', 'metadata_workflow',
                  'segmentation_workflow', 'transcript']


class TestimonyWorkflowSerializer(serializers.ModelSerializer):
    files_workflow = WorkflowSerializer(required=False)
    metadata_workflow = WorkflowSerializer(required=False)
    segmentation_workflow = WorkflowSerializer(required=False)
    transcript_workflow = serializers.SerializerMethodField()

    # transcript_public = serializers.SerializerMethodField()

    class Meta:
        model = Testimony
        fields = [#'id',
                  'files_workflow', 'metadata_workflow', 'segmentation_workflow', 'transcript_workflow',
                  #'files_public', 'metadata_public', 'segmentation_public', 'transcript_public'
                ]

    def get_transcript_workflow(self, obj):
        serializer = WorkflowSerializer(obj.transcript.workflow)
        return serializer.data

    def get_transcript_public(self, obj):
        return obj.transcript.public


# TODO review this. Using BaseSerializer instead of ModelSerializer is non-ideal
class TestimonyCompleteWorkflowSerializer(serializers.BaseSerializer):
    def to_representation(self, obj):
        return [
             {
                "content_type": "files",
                "public": obj.files_public,
                "workflow": WorkflowSerializer(obj.files_workflow).data
            },
            {
                "content_type": "metadata",
                "public": obj.metadata_public,
                "workflow": WorkflowSerializer(obj.metadata_workflow).data
            },
            {
                "content_type": "segmentation",
                "public": obj.segmentation_public,
                "workflow": WorkflowSerializer(obj.segmentation_workflow).data
            },
            {
                "content_type": "transcript",
                "public": obj.transcript.public,
                "workflow": WorkflowSerializer(obj.transcript.workflow).data
            }
        ]


class TestimonyHistorySerializer(serializers.ModelSerializer):
    change_user = serializers.SerializerMethodField()
    change_fields = serializers.SerializerMethodField()

    class Meta:
        model = Testimony.history.model
        fields = ['history_id', 'change_user', 'change_fields', 'history_user', 'history_date', ]

    def get_change_user(self, obj):
        return str(obj.history_user) if obj.history_user else None

    def get_change_fields(self, obj):
        try:
            delta = obj.diff_against(obj.prev_record)
            return [Testimony.history.model._meta.get_field(field).verbose_name for field in delta.changed_fields]
        except TypeError: # if prev_record is None
            return []
