# Generated by Django 2.1.7 on 2019-08-21 14:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('testimonies', '0010_auto_20190821_1454'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testimony',
            name='transcript',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='transcripts.Transcript', verbose_name='transcript'),
        ),
    ]
