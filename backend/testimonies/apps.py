from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class TestimoniesConfig(AppConfig):
    name = 'testimonies'
    verbose_name = _('testimonies')