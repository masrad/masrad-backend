import logging
from datetime import datetime

from django.shortcuts import render
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponse

from rest_framework import viewsets, permissions, status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import FormParser, MultiPartParser, JSONParser
from rest_framework.decorators import action

from sendfile import sendfile

from .models import Testimony, Collection, Person, Publisher
from workflow.models import Workflow

from .serializers import TestimonySerializer, TestimonyHistorySerializer, CollectionSerializer, \
                         PersonSerializer, PublisherSerializer, TestimonyWorkflowSerializer, TestimonyCompleteWorkflowSerializer
from workflow.serializers import WorkflowSerializer

from attachments.serializers import AttachmentSerializer
from transcripts.serializers import TranscriptSerializer
from segmentation.serializers import SegmentSerializer


class TestimoniesViewSet(viewsets.ModelViewSet):
    queryset = Testimony.objects.all()
    serializer_class = TestimonySerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly, )
    parser_classes = (MultiPartParser, FormParser, JSONParser, )

    @action(detail=True, methods=['patch',])
    def upload_file(self, request, pk=None):
        """
        Upload from file with a PATCH request containing 'Content-Type': 'multipart/form-data'
        #TODO check that the file is of the right format
        """
        return self.partial_update(request)

    @action(detail=True, methods=['patch',])
    def remove_file(self, request, pk=None):
        testimony = self.get_object()
        if request.data.get('file', None):
            testimony.file.delete()
        else:
            testimony.anonymized_file.delete()
        testimony.save()
        return Response({'status': 'file removed'})

    @action(detail=True, methods=['get', 'post'])
    def attachments(self, request, pk=None):
        if request.method == 'GET':
            testimony = self.get_object()
            attachments = testimony.attachments.all()
            serializer = AttachmentSerializer(attachments, many=True)
            return Response(serializer.data)
        else: # it's a POST
            # create an attachment
            serializer = AttachmentSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            attachment = serializer.save()

            # attach it to the testimony
            testimony = self.get_object()
            attachment_content_type = ContentType.objects.get_for_model(testimony)
            attachment.object_id = testimony.id
            attachment.content_type = attachment_content_type
            attachment.save()
            return Response(AttachmentSerializer(attachment).data, status=status.HTTP_201_CREATED)

    @action(detail=True, methods=['get', ])
    def transcript(self, request, pk=None):
        testimony = self.get_object()
        transcript = testimony.transcript
        serializer = TranscriptSerializer(transcript)
        return Response(serializer.data)

    @action(detail=True, methods=['get', ])
    def segments(self, request, pk=None):
        testimony = self.get_object()
        segments = testimony.segment_set.all().order_by('start')
        serializer = SegmentSerializer(segments, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['get', ])
    def history(self, request, pk=None):
        testimony = self.get_object()
        history = testimony.history.all().order_by('-history_date')
        serializer = TestimonyHistorySerializer(history, many=True)
        # FIXME: dirtily filter empty change records
        data = [record for record in serializer.data if record['change_fields']]
        return Response(data)

    @action(detail=True, methods=['post', ])
    def restore(self, request, pk=None):
        testimony = self.get_object()
        history_id = request.data['history_id']
        history = testimony.history.get(history_id=history_id)
        history.instance.save()
        return Response({"restored": True})

    @action(detail=True, methods=['get', ])
    def workflows(self, request, pk=None):
        testimony = self.get_object()
        files_workflow_id = testimony.files_workflow.id
        metadata_workflow_id = testimony.metadata_workflow.id
        segmentation_workflow = testimony.segmentation_workflow_id
        transcript_workflow = testimony.transcript.workflow.id
        workflows = [files_workflow_id, metadata_workflow_id, segmentation_workflow, transcript_workflow]
        workflow_queryset = Workflow.objects.filter(id__in=workflows)
        serializer = WorkflowSerializer(workflow_queryset, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['get', ])
    def workflows_complete(self, request, pk=None):
        testimony = self.get_object()
        serializer = TestimonyCompleteWorkflowSerializer(testimony)
        return Response(serializer.data)


class CollectionViewSet(viewsets.ModelViewSet):
    queryset = Collection.objects.all()
    serializer_class = CollectionSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly, )

    @action(detail=True, methods=['get', ])
    def testimonies(self, request, pk=None):
        collection = self.get_object()
        testimonies = collection.testimony_set.all()
        serializer = TestimonySerializer(testimonies, many=True)
        return Response(serializer.data)


class CreatorsViewSet(viewsets.ModelViewSet):
    serializer_class = PersonSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly, )

    def get_queryset(self):
        creators_set = set(Testimony.objects.filter(creator__isnull=False).values_list('creator', flat=True))
        return Person.objects.filter(id__in=creators_set)


class ContributorsViewSet(viewsets.ModelViewSet):
    serializer_class = PersonSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly, )

    def get_queryset(self):
        controbiutors_set = set(Testimony.objects.filter(creator__isnull=False).values_list('contributor', flat=True))
        return Person.objects.filter(id__in=controbiutors_set)


class PersonViewSet(viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly, )


class PublisherViewSet(viewsets.ModelViewSet):
    queryset = Publisher.objects.all()
    serializer_class = PublisherSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly, )


class ListContentLanguages(APIView):

    def get(self, request, format=None):
        """
        Return a list of usable languages
        """
        languages = settings.LANGUAGES
        return Response(languages)


class ListLicenseOptions(APIView):

    def get(self, request, format=None):
        """
        Return a list of license options
        """
        licenses = settings.LICENSE_CHOICES
        return Response(licenses)


def media_file(request, filename):
    """
    publishing status ->       |                      |
    visibility                 |                      |
      |                        |       draft          |     published
      v                        |                      |
    --------------------------------------------------------------------------------
        Public                 |    admins only       |    anonymous users
    --------------------------------------------------------------------------------
        Private                |    admins only       |    admins, researchers
                               |                      |
    """
    protected_file_path = 'protected/{0}'.format(filename)
    full_file_path = '{0}/protected/{1}'.format(settings.MEDIA_ROOT, filename)

    response = HttpResponse()
    response['X-Accel-Redirect'] = f'/protected/{filename}'

    # test out the permissions
    try:
        testimony = Testimony.objects.get(Q(file=protected_file_path) | Q(anonymized_file=protected_file_path) |  Q(release_form=protected_file_path))
    except Testimony.DoesNotExist:
        raise Http404
    # all the possible cases:
    if not testimony.published:
    #  CASE 1, 2: if the testimony is in draft mode
        if request.user.is_staff:
            return response
        else:
            raise PermissionDenied

    else:
    # CASE 3, 4: if the testimony is published
        if testimony.files_public:
            return response
        else:
            if request.user.is_authenticated:
                # both staff and researchers are authenticated users, so it's ok to use athentication as proxy for permission
                return response
            else:
                # out of curiosity/security, log the request
                logger = logging.getLogger('media_access')
                logger.log(20, '{} -- {} -- {} -- {}'.format(datetime.now(),
                                                            request.META['REMOTE_ADDR'],
                                                            request.path,
                                                            request.META['HTTP_USER_AGENT']))
                raise PermissionDenied


# from django.test.client import RequestFactory
# from testimonies.views import media_file
# from django.contrib.auth.models import User
# u = User.objects.get(id=1)
# rf = RequestFactory()
# r = rf.get('/media/')
# r.user = u
# rs=media_file(r, 'd7fed9a1a6c74a85beaccede9a80703f.png')
# rs.get('X-Accel-Redirect')
