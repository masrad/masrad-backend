import magic 
import uuid 

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.admin.models import ADDITION, CHANGE, LogEntry
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
from django.conf import settings
from django.template.defaultfilters import filesizeformat

from tags.models import Keyword, Subject 
from workflow.models import Workflow
from attachments.models import Attachment
from transcripts.models import Transcript

def file_name_uuid(instance, filename):
    extension = filename.split('.')[-1]
    return '{0}/{1}.{2}'.format(settings.PROTECTED_PREFIX, uuid.uuid4().hex, extension)


class Person(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('name'))
    identifier = models.CharField(max_length=50, verbose_name=_('identifier'))
    date_of_birth = models.DateField(blank=True, null=True, verbose_name=_('date of birth'))
    address = models.TextField(blank=True, null=True, verbose_name=_('address'))
    phone = models.CharField(blank=True, null=True, max_length=30, verbose_name=_('phone'))
    alternative_communication = models.TextField(blank=True, null=True, verbose_name=_('alternative communication'))

    class Meta:
        verbose_name = _('person')
        verbose_name_plural = _('persons')

    def __str__(self):
        return self.name

    def initials(self):
        try:
            initials = ''.join([name[0] for name in self.name_en.split()])
            return initials.upper()
        except AttributeError: # if the field is not provided in English
            return '__'

    def save(self, *args, **kwargs):
        # Save once so that the item has an ID that you can use in the identifier 
        super(Person, self).save(*args, **kwargs)
        # Save again now with an identifier
        self.identifier = '{initials}{id}'.format(**{'initials': self.initials(), 'id': self.id})
        super(Person, self).save()


class Publisher(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('name'))

    class Meta:
        verbose_name = _('publisher')
        verbose_name_plural = _('publishers')

    def __str__(self):
        return self.name


class Collection(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('name'))
    identifier = models.CharField(max_length=3, verbose_name=_('collection identifier'))

    class Meta:
        verbose_name = _('collection')
        verbose_name_plural = _('collections')

    def __str__(self):
        return self.name


class Restriction(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('name'))

    class Meta:
        verbose_name = _('restriction')
        verbose_name_plural = _('restrictions')

    def __str__(self):
        return self.name


class Testimony(models.Model):
    title = models.CharField(max_length=100, verbose_name=_('title'))
    file = models.FileField(upload_to=file_name_uuid, max_length=255, blank=True, null=True, verbose_name=_('audio recording file'))
    anonymized_file = models.FileField(upload_to=file_name_uuid, max_length=255, blank=True, null=True, verbose_name=_('anonymized audio recording file'))
    contributor = models.ForeignKey('Person', blank=True, null=True, on_delete=models.CASCADE, related_name='contributor', verbose_name=_('interviewer'))
    creator = models.ForeignKey('Person', blank=True, null=True, on_delete=models.CASCADE, related_name='creator', verbose_name=_('interviewee'))
    publisher = models.ForeignKey('Publisher', blank=True, null=True, on_delete=models.CASCADE, verbose_name=_('publisher'))
    identifier = models.CharField(max_length=50, blank=True, null=True, verbose_name=_('identifier'))
    date = models.DateTimeField(blank=True, null=True, verbose_name=_('interview date'))
    location = models.CharField(max_length=50, blank=True, null=True, verbose_name=_('interview location'))
    coverage = models.TextField(blank=True, null=True, verbose_name=_('coverage')) # Maybe use https://github.com/DinoTools/python-overpy
    description = models.TextField(blank=True, null=True, verbose_name=_('summary'))
    relation = models.ForeignKey('Collection', blank=True, null=True, on_delete=models.CASCADE, verbose_name=_('collection'))
    subject = models.ManyToManyField(Subject, blank=True, verbose_name=_('subjects'))
    file_format = models.CharField(max_length=255, blank=True, null=True, verbose_name=_('file format'))
    language = models.CharField(max_length=30, blank=True, null=True, choices=settings.LANGUAGES, verbose_name=_('original language')) 
    rights = models.CharField(default=settings.DEFAULT_LICENSE, max_length=30, blank=True, null=True, choices=settings.LICENSE_CHOICES, verbose_name=_('publishing rights'))
    _type = models.CharField(max_length=30, blank=True, null=True, default='Oral history interview', verbose_name=_('content type'))
    keywords = models.ManyToManyField(Keyword, blank=True, verbose_name=_('keywords'))
    # source = models.CharField(max_length=30) # This field is part of the DC spec, but we're not using it

    #transcript stuff
    transcript = models.OneToOneField(Transcript, blank=True, null=True, on_delete=models.CASCADE, verbose_name=_('transcript'))

    publication_restriction = models.ManyToManyField('Restriction', blank=True, verbose_name=_('restrictions'))
    release_form = models.FileField(upload_to=file_name_uuid, blank=True, null=True, verbose_name=_('scan of release form'))
    release_date = models.DateField(blank=True, null=True, verbose_name=_('release date'))

    # data hygiene
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True, verbose_name=_('created at'))
    modified = models.DateTimeField(auto_now=True, blank=True, null=True, verbose_name=_('last modified at'))
    attachments = GenericRelation(Attachment)

    # publishing data
    published = models.BooleanField(default=False, verbose_name=_('published'))
    files_public = models.BooleanField(default=False, verbose_name=_('files public visibility'))
    metadata_public = models.BooleanField(default=False, verbose_name=_('metadata public visibility'))
    segmentation_public = models.BooleanField(default=False, verbose_name=_('segmentation public visibility'))
    # TODO: think about moving transcript public status and workflow status to here

    # workflow status 
    files_workflow = models.OneToOneField(Workflow, blank=True, null=True, on_delete=models.CASCADE, verbose_name=_('files workflow'), related_name='files_workflow')
    metadata_workflow = models.OneToOneField(Workflow, blank=True, null=True, on_delete=models.CASCADE, verbose_name=_('metadata workflow'), related_name='metadata_workflow')
    segmentation_workflow = models.OneToOneField(Workflow, blank=True, null=True, on_delete=models.CASCADE, verbose_name=_('segmentation workflow'), related_name='segmentation_workflow')

    class Meta: 
        verbose_name = _('testimony')
        verbose_name_plural = _('testimonies')

    def __str__(self):
        return self.title

    def created_by(self): 
        testimony_type = ContentType.objects.get_for_model(self)
        added_log = LogEntry.objects.get(action_flag=ADDITION, object_id=self.id, content_type=testimony_type)
        return added_log.user
    created_by.short_description = _('created by')

    def file_size(self): 
        try:
            return filesizeformat(self.file.file.size)
        except ValueError:
            return None
        except FileNotFoundError:
            return None
    file_size.short_description = _('file_size')

    def anonymized_file_size(self): 
        try:
            return filesizeformat(self.anonymized_file.file.size)
        except ValueError:
            return None
        except FileNotFoundError:
            return None
    anonymized_file_size.short_description = _('anonymized_file_size')

    def last_changed_by(self): 
        testimony_type = ContentType.objects.get_for_model(self)
        changed_log = LogEntry.objects.filter(action_flag=CHANGE, object_id=self.id, content_type=testimony_type).last()
        if changed_log:
            return changed_log.user
        else:
            return changed_log
    last_changed_by.short_description = _('last changed by')

    def visibility(self):
        return self.public or self.published

    def save(self, *args, **kwargs):
        '''
        on save
        - create a workflow status instance if it doesn't exist already
        - automatically add: file format, identifier
        - convert file type to a unified format #TODO 

        '''
        # Save first to make sure that all required data is there
        if not self.files_workflow: 
            # the frontend assumes that the workflow object is always present. 
            self.files_workflow = Workflow.objects.create()

        if not self.metadata_workflow: 
            # the frontend assumes that the workflow object is always present. 
            self.metadata_workflow = Workflow.objects.create()

        if not self.segmentation_workflow: 
            # the frontend assumes that the workflow object is always present. 
            self.segmentation_workflow = Workflow.objects.create()

        if not self.transcript:
            # the frontend assumes that the transcript object is always present. 
            self.transcript = Transcript.objects.create()

        if self.file:
            super(Testimony, self).save(*args, **kwargs)
            self.file_format = magic.from_file(self.file.path)

        try: 
            code_parts = dict()
            code_parts['collection_code'] = self.relation.identifier
            code_parts['year'] = self.date.year
            code_parts['month'] = self.date.month
            code_parts['day'] = self.date.day
            code_parts['contributor'] = self.contributor.identifier
            code_parts['id'] = self.id
            self.identifier = '{collection_code}{year}{month}{day}{contributor}{id}'.format(**code_parts)
        except:
            pass
            
        super(Testimony, self).save(*args, **kwargs)
