from django import forms
from django.contrib import admin

class TestimonyAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TestimonyAdminForm, self).__init__(*args, **kwargs)
        self.fields['keywords'].widget = admin.widgets.AutocompleteSelectMultiple()