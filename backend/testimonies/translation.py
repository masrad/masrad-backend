from modeltranslation.translator import translator, TranslationOptions
from .models import Person, Publisher, Collection, Restriction, Testimony
import simple_history

class PersonTranslationOptions(TranslationOptions):
    fields = ('name', )


class PublisherTranslationOptions(TranslationOptions):
    fields = ('name', )


class CollectionTranslationOptions(TranslationOptions):
    fields = ('name', )

    
class RestrictionTranslationOptions(TranslationOptions):
    fields = ('name', )

    
class TestimonyTranslationOptions(TranslationOptions):
    fields = ('title', 'description', )


translator.register(Person, PersonTranslationOptions)
translator.register(Publisher, PublisherTranslationOptions)
translator.register(Collection, CollectionTranslationOptions)
translator.register(Restriction, RestrictionTranslationOptions)
translator.register(Testimony, TestimonyTranslationOptions)

simple_history.register(Testimony, excluded_fields=['created', 'modified', ])