from django.urls import reverse
from django.contrib.auth.models import User 
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.forms import model_to_dict

from rest_framework import status
from rest_framework.test import APITestCase, force_authenticate

from .models import Testimony
from .views import TestimoniesViewSet


class TestimonyTests(APITestCase):
    def setUp(self):
        self.username = 'auser'
        self.password = 'this is a password'
        self.user = User.objects.create_user(username=self.username, password=self.password, is_superuser=True)

        self.weak_username = 'weak_user'
        self.weak_password = 'this is a password'
        self.weak_user = User.objects.create_user(username=self.weak_username, password=self.weak_password)

        Testimony.objects.create(title='first testimony')

    def test_create_testimony_from_title(self):
        """
        Creating a testimony
        """
        self.client.force_authenticate(self.user)
        url = reverse('testimony-list')
        data = {'title': 'a testimony title'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Testimony.objects.count(), 2)
        self.assertEqual(Testimony.objects.get(pk=2).title, 'a testimony title')

    def test_change_testimony_anonymously(self):
        url = reverse('testimony-detail', args=[1])
        data = {'title': 'new title'}
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_change_testimony_without_permission(self):
        self.client.force_authenticate(self.weak_user)
        url = reverse('testimony-detail', args=[1])
        data = {'title': 'new title'}
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_upload_file(self):
        """
        uploading original testimony file
        """
        self.client.force_authenticate(self.user)
        url = reverse('testimony-detail', args=[1])
        file = SimpleUploadedFile('file.mp3', b'file_content', content_type='audio/mp3')
        ## Original file 
        response = self.client.patch(url, {'file': file}, content_type='multipart/form-data')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        testimony = Testimony.objects.get(id=1)
        self.assertIsNotNone(testimony.file)
        ## Anonymized file
        response = self.client.patch(url, {'anonymized_file': file}, content_type='multipart/form-data')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        testimony = Testimony.objects.get(id=1)
        self.assertIsNotNone(testimony.anonymized_file)

    def test_delete_file(self):
        """
        uploading original testimony file
        """
        self.client.force_authenticate(self.user)
        url = reverse('testimony-detail', args=[1]) + 'remove_file/'
        # view = TestimoniesViewSet()
        # url = view.reverse_action('remove-file', args=[1])
        response = self.client.patch(url, {'file': None}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        testimony = Testimony.objects.get(id=1)
        self.assertFalse(bool(testimony.file))

    def test_workflow_in_testimony(self):
        """
        Make sure that a testimony always has a workflow
        """
        self.client.force_authenticate(self.user)
        url = reverse('testimony-detail', args=[1])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        testimony = Testimony.objects.get(id=1)
        self.assertIsNotNone(testimony.workflow)
        self.assertEqual(model_to_dict(testimony.workflow), dict(response.data['workflow']))


class LanguagesTests(APITestCase):
    def test_content_languages_list(self):
        """
        Test list of content languages
        """
        url = reverse('content-languages')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(settings.LANGUAGES, response.data)
