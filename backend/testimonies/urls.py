from django.urls import path
from .views import TestimoniesViewSet, CollectionViewSet, ListContentLanguages, \
				   CreatorsViewSet, ContributorsViewSet, PersonViewSet, PublisherViewSet, \
			   	   ListLicenseOptions

from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'testimonies', TestimoniesViewSet)
router.register(r'collections', CollectionViewSet)
router.register(r'creators', CreatorsViewSet, basename='creators')
router.register(r'contributors', ContributorsViewSet, basename='contributors')
router.register(r'persons', PersonViewSet)
router.register(r'publishers', PublisherViewSet)

urlpatterns = router.urls

urlpatterns += [
    path('content-languages', ListContentLanguages.as_view(), name='content-languages'),
    path('license-options', ListLicenseOptions.as_view(), name='license-options'),
]