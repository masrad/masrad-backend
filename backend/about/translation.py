from modeltranslation.translator import translator, TranslationOptions
from .models import About

class AboutTranslationOptions(TranslationOptions):
    fields = ('name', 'description', 'long_description', )


translator.register(About, AboutTranslationOptions)