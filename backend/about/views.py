from django.shortcuts import render

from .models import About 
from .serializers import AboutSerializer

from rest_framework import generics, permissions, exceptions
from rest_framework.response import Response


class AboutView(generics.GenericAPIView):
    queryset = About.objects.all()
    serializer_class = AboutSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly, )
    def get(self, request, *args, **kwargs):
        instance = self.queryset.last()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)