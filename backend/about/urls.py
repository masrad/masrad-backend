from django.urls import path
from .views import AboutView 


urlpatterns = [
    path('about/<int:pk>/', AboutView.as_view()),
]