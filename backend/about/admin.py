from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from modeltranslation.admin import TranslationAdmin

from .models import About

class AboutAdmin(TranslationAdmin):
	# TODO think about adding pagedown markdown editor for those fields: https://github.com/timmyomahony/django-pagedown
	# TODO find a way to create a singleton admin, either using django-solo or or mezzanine https://github.com/gbezyuk/django-solo-grappelli 
    pass


admin.site.register(About, AboutAdmin)