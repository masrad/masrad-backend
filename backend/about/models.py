from django.db import models
from django.utils.translation import gettext_lazy as _

class About(models.Model):
    name = models.CharField(max_length=200, verbose_name=_('archive name'))
    logo = models.ImageField(upload_to='logo/', null=True, blank=True, verbose_name='archive logo')
    description = models.TextField(verbose_name=_('archive short description'))
    long_description = models.TextField(verbose_name=_('archive long description'))

    class Meta:
        verbose_name = _('about')
        verbose_name_plural = _('abouts')

    def __str__(self):
        return self.name
