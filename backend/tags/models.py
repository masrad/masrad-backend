from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.text import slugify


class Tag(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('name'))
    slug = models.SlugField(max_length=50, verbose_name=_('slug'))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_('created on'))
    
    class Meta:
        abstract = True

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name_en)
        super(Tag, self).save(*args, **kwargs)


class Keyword(Tag):
    class Meta:
        verbose_name = _('keyword')
        verbose_name_plural = _('keywords')


class Subject(Tag):
    class Meta:
        verbose_name = _('subject')
        verbose_name_plural = _('subjects')

