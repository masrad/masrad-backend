from rest_framework import serializers

from .models import Keyword, Subject

        
class KeywordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Keyword
        exclude = ['created', 'slug']
        
class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        # exclude = ['created', 'slug']
        fields = ['id', 'name', ]