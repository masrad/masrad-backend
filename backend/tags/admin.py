from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from modeltranslation.admin import TranslationAdmin

from .models import Subject, Keyword


class TagAdmin(TranslationAdmin):
    search_fields = ('name', )
    list_display = ('name', )
    readonly_fields = ('slug', )


admin.site.register(Subject, TagAdmin)
admin.site.register(Keyword, TagAdmin)