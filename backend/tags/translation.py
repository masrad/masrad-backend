from modeltranslation.translator import translator, TranslationOptions
from .models import Keyword, Subject


class TagTranslationOptions(TranslationOptions):
    fields = ('name', )


translator.register(Keyword, TagTranslationOptions)
translator.register(Subject, TagTranslationOptions)