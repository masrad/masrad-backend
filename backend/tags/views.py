from django.shortcuts import render
from rest_framework import viewsets, permissions, status

from .models import Keyword, Subject
from .serializers import SubjectSerializer, KeywordSerializer

class SubjectViewSet(viewsets.ModelViewSet):
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly, )


class KeywordViewSet(viewsets.ModelViewSet):
    queryset = Keyword.objects.all()
    serializer_class = KeywordSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly, )