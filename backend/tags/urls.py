from django.urls import path
from .views import KeywordViewSet, SubjectViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'keywords', KeywordViewSet)
router.register(r'subjects', SubjectViewSet)

urlpatterns = router.urls