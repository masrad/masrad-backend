from django.urls import path
from .views import TranscriptViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'transcripts', TranscriptViewSet)

urlpatterns = router.urls