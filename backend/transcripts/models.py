from django.db import models
from django.utils.translation import gettext_lazy as _

#from testimonies.models import Testimony
from workflow.models import Workflow

class Transcript(models.Model):
    # testimony = models.OneToOneField(TestiFmony, on_delete=models.CASCADE, verbose_name=_('testimony'))
    text = models.TextField(verbose_name=_('transcript'))
    workflow = models.OneToOneField(Workflow, null=True, on_delete=models.CASCADE, verbose_name=_('workflow'))
    public = models.BooleanField(default=False, verbose_name=_('transcript public visibility'))

    # data hygiene
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True, verbose_name=_('created at'))
    modified = models.DateTimeField(auto_now=True, blank=True, null=True, verbose_name=_('last modified at'))

    class Meta:
        verbose_name = _('transcript')
        verbose_name_plural = _('transcripts')

    def __str__(self):
        return self.text[:30]

    def save(self, *args, **kwargs):
        if not self.workflow: 
            # the frontend assumes that the workflow object is always present. 
            self.workflow = Workflow.objects.create()
        super(Transcript, self).save(*args, **kwargs)