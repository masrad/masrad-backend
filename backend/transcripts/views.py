from rest_framework import viewsets, permissions

from .models import Transcript
from .serializers import TranscriptSerializer

class TranscriptViewSet(viewsets.ModelViewSet):
    queryset = Transcript.objects.all()
    serializer_class = TranscriptSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly, )