from rest_framework import serializers

from workflow.serializers import WorkflowSerializer

from .models import Transcript

        
class TranscriptSerializer(serializers.ModelSerializer):
    workflow = WorkflowSerializer(required=False)

    class Meta:
        model = Transcript
        fields = ['id', 'text', 'workflow', 'public', ]