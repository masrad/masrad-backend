from django.contrib import admin

from modeltranslation.admin import TranslationAdmin, TranslationStackedInline

from .models import Transcript


class TranscriptInline(TranslationStackedInline):
    model = Transcript
    max_num = 1
    extra = 0

class TranscriptAdmin(TranslationAdmin):
    pass

# admin.site.register(Transcript, TranscriptAdmin)