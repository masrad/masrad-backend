from modeltranslation.translator import translator, TranslationOptions
from .models import Transcript

class TranscriptTranslationOptions(TranslationOptions):
    fields = ('text', )


translator.register(Transcript, TranscriptTranslationOptions)