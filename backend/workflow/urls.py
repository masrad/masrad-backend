from django.urls import path

from rest_framework import routers

from .views import WorkflowViewSet

router = routers.DefaultRouter()
router.register(r'workflows', WorkflowViewSet)

urlpatterns = router.urls