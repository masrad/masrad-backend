from modeltranslation.translator import translator, TranslationOptions
from .models import Workflow

class WorkflowTranslationOptions(TranslationOptions):
    fields = ('content_complete', 'anonymization_complete', 'quality_control_complete')


translator.register(Workflow, WorkflowTranslationOptions)