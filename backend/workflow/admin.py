from django.utils.translation import gettext_lazy as _
from django.contrib import admin

from modeltranslation.admin import TranslationAdmin

from .models import Workflow


class WorkflowInline(admin.StackedInline):
    model = Workflow
    max_num = 1
    extra = 0


class WorkflowAdmin(TranslationAdmin):    
    fieldsets = (
        (_('workflow status'), {
            'fields': ('content_complete', 'anonymization_complete', 'quality_control_complete')
        }),
        (_('house keeping'), {
            'fields': ('auto_populate_translations',),
        }),
    )
    list_display = ('content_complete', 'anonymization_complete', 'quality_control_complete', )


admin.site.register(Workflow, WorkflowAdmin)
