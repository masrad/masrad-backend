from rest_framework import serializers

from .models import Workflow

        
class WorkflowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Workflow
        fields = ['id', 'content_complete', 'anonymization_complete', 'quality_control_complete']
        