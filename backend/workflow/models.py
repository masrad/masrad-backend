from django.utils.translation import gettext_lazy as _
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models

from modeltranslation.utils import auto_populate

class Workflow(models.Model):
    content_complete = models.BooleanField(default=False, verbose_name=_('content complete'))
    anonymization_complete = models.BooleanField(default=False, verbose_name=_('anonymization complete'))
    quality_control_complete = models.BooleanField(default=False, verbose_name=_('quality control complete'))

    # house keeping
    auto_populate_translations = models.BooleanField(default=False, verbose_name=_('automatically populate translations'))

    class Meta:
        verbose_name = _('workflow')
        verbose_name_plural = _('workflows')

    def __str__(self):
        #return _('Content complete: {0}, Anonymization complete: {1}, Quality control complete: {2}'.format(*[self.content_complete, self.anonymization_complete, self.quality_control_complete]))
        return 'Content complete: {0}, Anonymization complete: {1}, Quality control complete: {2}'.format(*[self.content_complete, self.anonymization_complete, self.quality_control_complete])

    def save(self, *args, **kwargs):
        # if auto_populate_translations, (used in the case of the files section)
        if self.auto_populate_translations:
            # FIXME: auto populate all fields if a field changes. Now we're relying on the fallback value.
            pass
        super(Workflow, self).save(*args, **kwargs)