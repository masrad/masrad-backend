from django.urls import path
from .views import SegmentViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'segments', SegmentViewSet)

urlpatterns = router.urls