from django.contrib import admin
from modeltranslation.admin import TranslationAdmin, TranslationStackedInline

from .models import Segment

class SegmentAdmin(TranslationAdmin):
    pass


class SegmentInline(TranslationStackedInline):
	# TODO find/create a nice widget for entering time
    model = Segment
    extra = 1
    autocomplete_fields = ['keywords', 'subject', ]


# admin.site.register(Segment, SegmentAdmin)
