from django.db import models
from django.utils.translation import gettext_lazy as _

from testimonies.models import Testimony
from tags.models import Subject, Keyword


class Segment(models.Model):
    testimony = models.ForeignKey(Testimony, on_delete=models.CASCADE, verbose_name=_('testimony'))
    start = models.DurationField(verbose_name=_('start'))
    title = models.CharField(max_length=250, blank=True, null=True, verbose_name=_('title'))
    summary = models.TextField(blank=True, null=True, verbose_name=_('summary'))
    subject = models.ManyToManyField(Subject, blank=True, verbose_name=_('subjects'))
    keywords = models.ManyToManyField(Keyword, blank=True, verbose_name=_('keywords'))
 
    class Meta:
        verbose_name = _('segment')
        verbose_name_plural = _('segments')

    def __str__(self):
        return self.title