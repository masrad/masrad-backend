from modeltranslation.translator import translator, TranslationOptions
from .models import Segment

class SegmentTranslationOptions(TranslationOptions):
    fields = ('title', 'summary', )

translator.register(Segment, SegmentTranslationOptions)
