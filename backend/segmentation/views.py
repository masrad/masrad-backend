from rest_framework import viewsets, permissions

from .models import Segment
from .serializers import SegmentSerializer

class SegmentViewSet(viewsets.ModelViewSet):
    queryset = Segment.objects.all()
    serializer_class = SegmentSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly, )