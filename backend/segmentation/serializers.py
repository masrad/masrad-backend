from rest_framework import serializers

from .models import Segment

        
class SegmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Segment
        fields = ['id', 'testimony', 'start', 'title', 'summary', 'subject', 'keywords']