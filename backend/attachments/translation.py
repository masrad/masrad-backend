from modeltranslation.translator import translator, TranslationOptions
from .models import Attachment


class AttachmentTranslationOptions(TranslationOptions):
    fields = ('name', 'description', )

translator.register(Attachment, AttachmentTranslationOptions)