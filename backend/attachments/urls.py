from django.urls import path
from .views import AttachmentViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'attachments', AttachmentViewSet)

urlpatterns = router.urls