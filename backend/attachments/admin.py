from modeltranslation.admin import TranslationGenericStackedInline

from .models import Attachment


class AttachmentInline(TranslationGenericStackedInline):
    model = Attachment
    search_fields = ('name', 'description', )
    list_display = ('name', )
    extra = 1