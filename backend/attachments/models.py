from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import gettext_lazy as _


class Attachment(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True, verbose_name=_('name'))
    description = models.TextField(blank=True, null=True, verbose_name=_('description'))
    file = models.FileField(upload_to='protected/attachments/', max_length=255, blank=True, null=True, verbose_name=_('attached file'))
    
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True, verbose_name=_('created at'))
    modified = models.DateTimeField(auto_now=True, blank=True, null=True, verbose_name=_('last modified at'))

    content_type = models.ForeignKey(ContentType, null=True, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        verbose_name = _('attachment')
        verbose_name_plural = _('attachments')

    def __str__(self):
        return self.name or self.id