from rest_framework import viewsets, permissions

from .models import Attachment
from .serializers import AttachmentSerializer

class AttachmentViewSet(viewsets.ModelViewSet):
    queryset = Attachment.objects.all().order_by('-modified')
    serializer_class = AttachmentSerializer
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly, )