"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static

from rest_framework.authtoken import views

from testimonies.views import media_file

urlpatterns = [
    path('grappelli/', include('grappelli.urls')), # grappelli URLS
    path('api/', include('testimonies.urls')),
    path('api/', include('about.urls')),
    path('api/', include('users.urls')),
    path('api/', include('workflow.urls')),
    path('api/', include('tags.urls')),
    path('api/', include('attachments.urls')),
    path('api/', include('transcripts.urls')),
    path('api/', include('segmentation.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('api/token-auth/', views.obtain_auth_token),
    path('media/protected/<str:filename>', media_file)
]

urlpatterns += i18n_patterns(
    path('admin/', admin.site.urls),
)

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [
    	path('rosetta/', include('rosetta.urls'))
    ]
    
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)